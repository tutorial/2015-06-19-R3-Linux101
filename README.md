# Linux Basics 101 and GitLab R3 Workshop

## Quiz (to get to know you)

* Who doesn't know what an IP address is?
* Who doesn't know what a server is?
* Who doesn't know what a port is?


## How to connect to a remote Session
***

### 1. My terminal is better than yours?

> Definition: A terminal is an interface in which you can type and execute text based commands.

#### Download your Terminal if you don't have one already.


+ **Windows**
  + MobaXterm [download](http://mobaxterm.mobatek.net/download-home-edition.html)

  > Remarks: In Windows, there is a default command line, called command prompt. However, the names of many commands is different and some commands do not exist. For that reason, use another terminal, such as MobaXterm.

+ **Mac**
  + iTerm2 [download](https://www.iterm2.com/downloads.html)
  + Terminal
+ **Linux**
  + Terminal

### 2. Generate your SSH key

#### Background information

+ **What is SSH?**

<img src="images/screen3.png" width=600>

* **What is a SSH key pair?**

<img src="images/screen4.jpg" width=400>

#### Key generation

+ **Windows (MobaXterm) / Mac / Linux**
  + ssh-keygen

        ```bash
        vagrant@vagrant-ubuntu-trusty-64:~$ ssh-keygen
        Generating public/private rsa key pair.
        Enter file in which to save the key (/home/vagrant/.ssh/id_rsa):
        Enter passphrase (empty for no passphrase):
        Enter same passphrase again:
        Your identification has been saved in /home/vagrant/.ssh/id_rsa.
        Your public key has been saved in /home/vagrant/.ssh/id_rsa.pub.
        The key fingerprint is:
        ac:d7:36:ee:74:f6:34:d1:ff:ee:dd:bc:d5:ee:70:36 vagrant@vagrant-ubuntu-trusty-64
        The key\'s randomart image is:
        +--[ RSA 2048]----+
        |                 |
        |                 |
        |                 |
        |       .       . |
        |        S     . .|
        |       . .     .o|
        |      . . = o +E*|
        |       . + + o B*|
        |         .o   .=@|
        +-----------------+
        ```

  > Note, you must **NEVER** share the id_rsa file with anybody

### 3. Create LUMS account

This task has been done already by your awesome R3 team.

### 4. Add SSH key to LUMS (**L**CSB **U**ser **M**anagement **S**ystem)

* Attach your *public* SSH key to your LUMS account
* To get the contents of your public key, type the following command (you will understand it all later today :) )

```bash
cd
cat .ssh/id_rsa.pub
```

Expected output (your key will be different)

Question: Is it OK for me to post my public key publicly?

```
christophe.trefois.TrefexVMPC$ cd && cat .ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDRVvaaZejn/CHQsXdb2nMzzPP3WF14H9djaTWmOCabl/q1BJEjWX21P1D+r45mB1EUfUi5w1VrN6I3/9ytZweOa4kLsMJCYU0sEwZq2KKOEKkW7M7W6LGJ+4C2QzkXLCY4r2yToAK7HwvvDUoDF3Ioodw9omTFLaHGzTLEsw9KVrYYwcyAQ5SOQTsL3VuMgyTd+Z8414HPIQrB3abEqHGGvkcrxnKhaEF/R3NNJ523ErRmRZa9cIB6dhXjzEIcvgaDzMKtXbflciVt3u3vM+4aaKhHwO73KUv8wDiPau8ROxmvNkADZB4gpsjbKqVP876QCT5YI5Z0D8Bcj5GoKZ1D christophe.trefois@TrefexVMPC

```

* Go to https://lcsb-ipa.uni.lu
* Login with first.lastname and password linuxwork2015

    > (Note: Use your password for existing LUMS account)

* Next to SSH public keys, click on Add and copy paste the key in there, including `ssh-rsa` and the text at the end.

* Don't forget to click on *Update* on the top menu buttons.

### 5. Connecting to Virtual Machine

* **Windows / Mac / Linux**
  * Open your terminal
  * Connect to the workshop virtual machine

        ```bash
        ssh first.lastname@10.79.2.191 -p 8022
        ```

      Explanations:

      * `ssh` is the command we will use to connect to our server
      * `first.lastname` is the username we will use
      * `@10.79.2.191` is the IP address of the server itself
      * `-p 8022` is a parameter. Here it specifies the port to connect to.

## Before we start

* A word that looks like `this` means that you should replace the word `this` with appropriate content.

## Discover your surroundings
***
* In which folder am I? (Hint: pwd)
* What is the contents of the current folder? (Hint: ls)
* Creating my first file called myFile.txt (Hint: touch `filename`)
* Checking that my file exists (Hint: ls)
* Creating my first folder called "myFirstFolder" (Hint: mkdir `foldername`)
* How do I enter my new folder? (Hint: cd `foldername`)
* Copying my first file to the new folder (Hint: cp `source` `target`)

**Exercise 1**

Check if the file is in the correct folder.

    Hint: ls, pwd

What if I want to see a structure like in Windows Explorer?

--> Try this:

```bash
cd
tree
```

You should see something like this:

```bash
.
├── myFile.txt
└── myFirstFolder
    └── myFile.txt

1 directory, 2 files
```

**Exercise 2**

I see `myFile.txt` twice. Maybe I should remove the copy I don't want.

* Go to where the to-be-deleted file is (Hint: cd)
* Remove the file (Hint: rm -i `myFile.txt`)

> In Linux, when asked questions you usually type `y` for **Yes** and `n` for **No** unless explicitly written.

## A couple remarks on file / folder names
* Try not to use spaces
* Try to capitalize some words for easier reading
  * for instance: thislongfoldernameisnotveryreadable, but ThisLongFolderNameIsQuiteReadable

## Learn more about any *nix command
***
* Go to your home folder (Hint: cd <no arguments>)
* Are there any hidden files in my home folder? (Hint: man ls)

> A hidden fie or folder starts with a dot. Note, on Windows, you normally can't create folders that start with a dot but with mobaXterm you can :)

When you do `ls -al` you see a different view and many hidden files.

* How do I get such nice documentation for other unix commands? (Hint: man <mycommand>)

## Copy files to the VM or from the VM (only using console!!)
***
* Logout of the VM (Hint: logout)
* Create a local training folder `training` (Hint: mkdir `training`)
* Go to the new folder called `training` (Hint: cd)
* Create a new file on your computer (Hint: touch `file3.txt`)
* Copy file1.txt to “myFirstFolder” on the VM

  Hint:

  ```bash
  scp –P 8022 file3.txt first.lastname@1079.2.191:myFirstFolder/
  ```

  Explanations:

  * `scp` stands for `secure copy` and the basic syntax is `scp source target`
  * `-P 8022` specifies the port on which the server is accpeting our connection
  * `first.lastname@10.79.2.191` specifies how we want to login
  * `:/myFirstFolder` means the file will be put in to that folder on the server


* Connect back to the server and see if your new file is there

    Hint:

    ```bash
    ssh first.lastname@10.79.2.191 -p 8022
    cd myFirstFolder
    ls -al
    ```

## Command line tools and parameters
***

Let's do something fun!

On our server, we will download a zip file and unzip the contents. Ready? First, let's understand the zip command.

* Connect to the server
* Go to folder `myFirstFolder`
* Zip the two files you created

```bash
zip myArchive.zip myFile.txt file3.txt
```

* Check the contents of the zip archive

```bash
unzip -l myArchive.zip
```

Couple of notes:

* `-l` is a parameter. You can see what these parameters mean by doing `man unzip`.

Let's move on.

### Let us download a zip from the Internet and extract its contents

Let's go:

```bash
cd
wget http://bit.ly/anArchive -O myName.zip
ls
```

* First I check the contents to make sure it's what I wanted:

    ```
    unzip -l myName.zip
    ```

* If I am satisfied, I can safely extract

    ```
    unzip myName.zip
    ```

## Text editor
***
* nano [Tutorial](http://www.howtogeek.com/howto/42980/the-beginners-guide-to-nano-the-linux-command-line-text-editor/)

* On the server, open the file `thisfile.txt` that you extracted above.


```bash
nano thisfile.txt
```

* Simply start typing and changing contents...

> Do you realize you are currently changing a file in a remote server and it feels like you are using Notepad? Much awesome!

* When you are done, you can save by hitting `ctrl-o`, and then `Enter`.
* Now you can quit with `ctrl-x`

* There are many other text editors for terminal only, such as vi, vim and emacs to name a few.

## File System explore
***
### Midnight Commander `mc`

* Installing
  * Windows (MobaXterm) / Linux  `apt-get install mc`
  * MacOS X `brew install mc`

  > If you don't have brew, install it at: http://brew.sh/

* [Tutorial](http://linuxcommand.org/lc3_adv_mc.php)
* if function keys do not work, use `Esc`+`1` for `F1`, `Esc`+`2` for `F2`, ..., `Esc`+`9` for `F9` and `Esc`+`0` for `F10`
* if you do not have an `Insert` key (e.g. on Apple keyboards) use `Ctrl`+`t` for (un-)selecting files and folders

## Scripts - Running and Writing your own
***

Sometimes when you download and extract a zip file, it will contain not only files to look at, but also programs or scripts that you want to run.

Have a look at the contents extracted earlier and you will notice a file called `aprogram.sh`

* Run the program using `./aprogram.sh`

> This is a special syntax which tells the terminal to execute a program named `aprogram.sh` in the current folder rather than in the normal folders where all commands are. On Windows "Program Files" and *nix /usr/bin, /usr/sbin/ etc...

* Run the program again using `./aprogram.sh YourName`
* Look at the contents of the program using `nano`

## Create your first bash script!

* Create a new file called `myScript.sh`

> You can use touch and then nano, or you can use nano directly.

* Type in the following two lines

```bash
#!/bin/bash

echo "Hello World"
```

* Save and exit.
* Run the program using `./myScript.sh`
* ERROR. Why?

    We need to tell linux that a given file can be executed and therefore have to give the correct permissions to that file to transform it into a program that can be launched.
* To make our script executable do `chmod u+x myScript.sh`
* Launch our first script again


You may continue your quest in Bash using the following [tutotial](http://linuxconfig.org/bash-scripting-tutorial)

## Basic git (source control)
***
* add
* commit
* push
* pull
